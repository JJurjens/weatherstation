#include "hd44780.h"

#include <avr/io.h>

static void lcd_send(uint8_t byte);
static inline void pulse_enable();

void lcd_setup(){

	// Init procedure
	_delay_ms(INIT_SETUP_DELAY);	// wait for 20 miliseconds after VCC rises to 4.5V
	lcd_sendinstruction((1 << DB5) | (1 << DB4));

	_delay_ms(SECOND_SETUP_DELAY);	// wait for more 10 miliseconds
	lcd_sendinstruction((1 << DB5) | (1 << DB4));

	_delay_ms(LAST_SETUP_DELAY);	// wait for 1ms
	lcd_sendinstruction((1 << DB5) | (1 << DB4));

	// Initial function set
	lcd_sendinstruction(0x20);	// set to 4-bit operation

	// Real function set
	lcd_sendinstruction(0x28);	// set to 4-bit operation, 2 line display, 5x8 dot character?

	// Display on/of
	lcd_sendinstruction(0x0C);	// Display on, cursor off -> might need cursor on?

	// Entry mode
	lcd_sendinstruction(0x06);	// Sets mode to increment by 1
}

void lcd_sendinstruction(uint8_t instruction){

	ctrl_port &= ~((1 << RS) | (1 << RW));
	
	lcd_send(instruction);
}

void lcd_senddata(uint8_t data){

	ctrl_port |= ((1 << RS));
	ctrl_port &= ~((1 << RW));

	lcd_send(data);
}

static void lcd_send(uint8_t byte){

	lcd_waitbusy();

	// Set high nibble on port
	data_port = ((byte & 0xF0));
	pulse_enable();

	// Set low nibble on port
	data_port = ((byte & 0x0F) << 4);	
	pulse_enable();

}

void lcd_waitbusy(){

	_delay_ms(2);	
	return;

	uint8_t busy = 0;

	// set the 4 data_port pins to input
	data_ddr &= ~((1 << DB7) | (1 << DB6) | (1 << DB5) | (1 << DB4));
	
	// set RW pin high
	ctrl_port |= ((1 << RW));

	do{

		// set E pin high
		ctrl_port |= ((1 << E));

		// read data_port
		// deduct DB7
		pulse_enable();

	}while(1);
	
	// set RW pin high
	ctrl_port &= ~((1 << RW));

	// set 4 data_port pins to output
	data_ddr |= ((1 << DB7) | (1 << DB6) | (1 << DB5) | (1 << DB4));
}

void lcd_cleardisplay(){

	lcd_sendinstruction(0x01);
	_delay_ms(2);	
}

void lcd_clearline(uint8_t line){

}

void lcd_goto(uint8_t x, uint8_t y){

	if((x >= 0x00 && x <= 0x0F) && (y >= 0x00 && y <= 0x01)){
	
		lcd_sendinstruction(0x80 | x | (y << 6));	
	}
}

void lcd_printchar(char c){

	lcd_senddata(c);
}

void lcd_printstring(char *s){

	while(*s){

		lcd_printchar(*s++);
	}
}

static inline void pulse_enable(){

	ctrl_port |= ((1 << E));
	_delay_us(PULSE_DELAY);
	ctrl_port &= ~((1 << E));
}
