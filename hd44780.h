#ifndef hd44780_H_INCLUDED
#define hd44780_H_INCLUDED

#include <avr/io.h>
#include <util/delay.h>

// See page 24
// Function set					-> DB5 needs 1?
// DL 	= 0 	-> 4 bit			-> DB4
// N 	= 1	-> 2-line			-> DB3
// F	= 0	-> default 5x8 font		-> DB2

// Display on/of control			-> DB3 needs 1?
// D	= 1	-> display on			-> DB2
// C	= 0	-> cursor off			-> DB1
// B	= 0	-> blinking off			-> DB0

// Entry mode set				-> DB2 needs 1?
// I/D	= 1 	-> increment (address?) by 1	-> DB1
// S	= 0	-> no shift			-> DB0

#define data_port 		PORTD
#define ctrl_port 		PORTB

#define data_ddr		DDRD
#define ctrl_ddr		PORTB

#define DB7			PD7
#define DB6			PD6
#define DB5			PD5
#define DB4			PD4

#define RS			PB0
#define RW			PB1
#define E			PB2

#define PULSE_DELAY 		10
#define INIT_SETUP_DELAY	20
#define SECOND_SETUP_DELAY	10
#define LAST_SETUP_DELAY	1

void lcd_setup();
void lcd_sendinstruction(uint8_t instruction);
void lcd_senddata(uint8_t data);
void lcd_waitbusy();

void lcd_cleardisplay();
void lcd_clearline(uint8_t line);
void lcd_goto(uint8_t x, uint8_t y);

void lcd_printchar(char c);
void lcd_printstring(char *s);

#endif
