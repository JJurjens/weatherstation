#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include "hd44780.h"
#include <avr/interrupt.h>

/*
 * Main program of weather station trial
 * Step 1: Program display (driver) -> USE 4 or 8 Pins of PORTD
 * Step 2: Sensor readout through SPI/I2C (BME280)
 * Step 3: Timer based interrupt
 * Step 4: Deep sleep in between interrupts
*/

void init();

int main(void) {
	
	init();
	lcd_setup();

	lcd_cleardisplay();						// clear display and return to home

	while(1){ }

	return 0;
}

void init()
{
    cli();

	DDRD = 0xF0;							// Set 4 most significant pins of PORTD as output
	DDRB |= ((1 << PB0) | (1 << PB1) | (1 << PB2));			// Set RS, RW and E pins as output
		
	PORTD = 0x00; 							// Init LCD pins to all low
	PORTB = 0x00;							// Init control pins to all low
    
    // setup timer interrupt
    TCCR1A = 0;
    TCCR1B = 0;
    //TIMSK1 = 0;

    TCNT1 = 30000;                          // calculate timer preset value
    TCCR1B |= ((1 << CS12));// set prescaler if wanted
    TIMSK1 |= ((1 << TOIE1));              // enable timeroverflow interrupt

    sei();
}

ISR(TIMER1_OVF_vect)
{
    TCNT1 = 30000;
    
    static int counter = 0;

    lcd_goto(0, 0);
    lcd_printchar(counter);

    counter++;
    // do stuff
}