# DIY Solar powered wireless weather station

### Introduction

This project is the result of wanting to learn stuff and do stuff. I'm taking inspiration from [Solar powered WiFi weather station V2.0](https://hackaday.io/project/165061-solar-powered-wifi-weather-station-v20). Although for now, I plan to design all of the electronics myself.  

This project will also be used for my second planned project, which is sort a Info-hub/smart mirror thing that will probably run a Raspberry Pi (zero?) in combination with an E-Paper display or use a secondhand Kindle.

### What do I want to measure?

What's a weather station's use? Measuring stuff right? I'm no (hobby) meteorologist so I have no special or fancy requirements in terms of what I want to measure. The most important measurement for me is temperature. That being said, it would be nice to be able to measure humidity and barometric pressure. As these are no special requirements, there should be plenty of sensors that are able to do these measurements. One commonly used sensor is the Bosch BME280, which can do all of these measurements.

### What do I want to do with these measurements?

I guess you could do a bunch of things with the measurement results. I'm only interested in two things. First log the data so I could whatever I want in the future. The second thing is providing live data to the E-Paper project, for whenever that might get finished.  

### How do I want to log the data?

Preferrably in a database. I'm not quite sure which database one that will be. For now I'm thinking about using SQLite but in the end it doesn't really matter that much.

### How do I transfer this data?  

I want to transfer the data wirelessly. I'm not quite sure on what technology to use. This might be WiFi, Zigbee, Z-Wave or 2.4 GHz RF. As I'm planning on powering this whole weather station with a solar panel + battery combination it would make sense to pick something that will not draw to much power but still have enough range.

Some quick thoughts:  
NRF24L01 -> really low power + more than enough range.  
Microcontroller could be in deep sleep waiting for watchdog timer.  
On wake up it could wake up NRF. Or it could connect power to NRF by switching a FET.  

The receiving end could possibly be a arduino with ethernet shield, which can be woken up by a message received by the NRF and setting a pin which triggers an external interupt on the receiving arduino and wake it up.

Advantages of WiFi is that there is no need for the arduino with ethernet shield as the weather station itself could directly populate the database.

Downsides of WiFi is the increase of power that's needed. When transmitting data, WiFi could use 10 times more power than the NRF. That being said, that could just be solved by a bigger solar panel and/or battery.

### Powering the weather station  

As the title indicates I want this weather station to be solar powered and wireless so I can put this thing any place I want and/or might be best. Going this route requires the weather station to have some components. First the obvious, a solar panel. Secondly, a battery to hold the electrical charge which solar panel provided. Third, some kind of circuit that handles everything that's required for properly charging a battery. And finally, possibly some circuitry that protects the battery if that's not being handled by the charging circuit.

### What about the enclosure?  

As a weather station is doing measurements on it's direct surroundings it's important to take the thing that is enclosing it into account. Luckily someone already put a lot of thought into this and Mr. Stevenson created a enclosure screen (aka the Stevenson screen). This solution provides an optimal place for the measurements to be taken, if put in to the right location of course. My plan is to use Mr. Stevenson's idea for my own enclosure.
