#ifndef I2C_H
#define I2C_h

// TWI -> Two wire interface -> I2C
// SDA + SCL

// Address packet 9 bits -> 7 bits of adress msb to lsb, R/W, ACK
// Slave pulls 9 bit low for ACK?
// SLA+R or SLA+W => slave address read or slave address write
// Data packet 9 bits -> 1 byte of data msb to lsb, ACK (from slave?)

// Transmission:
// 	Start condition -> ???
// 	SLA + R/W
// 	1..* datapackets
// 	Stop condition -> ??
//

#endif