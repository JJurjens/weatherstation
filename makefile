# Name: Makefile
# Author: Jeroen Jurjens

# targets:
# all		: compiles the source code
# test		: tests the isp connection to the MCU
# flash		: writes the compiled hex file to the MCU's flash memory
# clean		: removes all .hex, .elf, and .o files in the source directory

# parameters:
PROJ		= main
DEVICE		= atmega328p
CLOCK		= 16000000
PROGRAMMER 	= -c arduino -P /dev/ttyACM0 -p $(DEVICE) -b 115200
FLASH		= -U flash:w:$(PROJ).hex
CLEAN		= rm $(PROJ).hex $(PROJ).o $(PROJ).elf

CC 		    = avr-gcc
CFLAGS		= -Os -DF_CPU=$(CLOCK)UL -mmcu=$(DEVICE)
OBJCOPY		= avr-objcopy -j .text -j .data -O ihex 
AVRDUDE 	= avrdude $(PROGRAMMER)

DISP        = hd44780
I2C         = i2c
DEPS		= $(DISP).h $(I2C).h
OBJ		    = $(PROJ).o $(DISP).o $(I2C).o

# user targets:
# compile all files
build : $(PROJ).hex
	
$(PROJ).hex : $(PROJ).elf
	$(OBJCOPY) $(PROJ).elf $(PROJ).hex

$(PROJ).elf: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
# -o $@ = -o followed by a name defines the name of the output file, $@ is the name, in this case the output file on the left of :
# $^	= the filenames of all the prerequisites

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)
# -c 	= generate the object file
# -o $@ = -o followed by a name defines the name of the output file, $@ is the name, in this case the output file on the left of :
# $< 	= the first prerequisite to create the output file, the corresponding .c file in this case (main.o and main.c)

# test programmer connectivity
test:
	$(AVRDUDE) -v

# flash program to MCU
flash: build
	$(AVRDUDE) $(FLASH)

clean:
	$ rm -f *.hex *.o *.elf
